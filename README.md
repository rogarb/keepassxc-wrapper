keepassxc-wrapper
=================

A TUI wrapper for keepassxc.

Use case
--------
I have a TUI program, curses-based, needing a password input.
Using keepassxc with secret service enabled works fine, but I want 
to have to possibility to start the program when the database is not 
unlocked, by using keepassxc-cli.
Since the TUI program is curses-based, it doesn't work well with plain
CLI programs asking for input on the commandline as keepassxc-cli does.
This program simply stands in the middle, asking for passwords if 
secret service is not answering.

The secret-tool and keepassxc-cli commands can be set with the 
SECRET_TOOL_COMMAND and KEEPASSXC_CLI_COMMAND environment variables,
respectively. If these environment variables are not set, the commands
will fall back to the default value of "secret-tool" and "keepassxc-cli",
respectively.
