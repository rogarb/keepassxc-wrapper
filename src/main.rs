use anyhow::anyhow;
use clap::Parser;
use cursive::traits::{Nameable, Resizable};
use cursive::views::{Dialog, EditView, LinearLayout, TextView};
use cursive::{Cursive, CursiveExt};
use std::cell::RefCell;
use std::env;
use std::fmt;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::process::{Command, Stdio};
use std::rc::Rc;

/// A wrapper for KeepassXC
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct KeepassxcWrapper {
    /// The path to the kdbx database file
    #[arg(value_parser(validate_kdbx))]
    keepassdb: String,
    /// The attribute used for lookup (must be set in advanced tab)
    attribute: String,
    /// The entry-specific value associated with the attribute
    value: String,
}

/// The environment variable to look for the secret-tool command
const SECRET_TOOL_COMMAND: &'static str = "SECRET_TOOL_COMMAND";
/// The environment variable to look for the keepassxc-cli command
const KEEPASSXC_CLI_COMMAND: &'static str = "KEEPASSXC_CLI_COMMAND";

#[derive(Debug)]
enum ValidationError {
    PathDoesNotExist,
    PathIsDirectory,
    PathIsNotReadable,
}

impl fmt::Display for ValidationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            Self::PathDoesNotExist => "Path does not exist",
            Self::PathIsDirectory => "Path is a directory",
            Self::PathIsNotReadable => "Path is not readable",
        };
        write!(f, "{msg}")
    }
}

impl std::error::Error for ValidationError {}

/// Ensures the input is an existing path
fn validate_kdbx(db: &str) -> Result<String, ValidationError> {
    let path = Path::new(db);
    if path.exists() {
        if path.is_file() {
            if File::open(path).is_ok() {
                Ok(db.into())
            } else {
                Err(ValidationError::PathIsNotReadable)
            }
        } else {
            Err(ValidationError::PathIsDirectory)
        }
    } else {
        Err(ValidationError::PathDoesNotExist)
    }
}

fn main() -> anyhow::Result<()> {
    let args = KeepassxcWrapper::parse();
    let secret_tool_command = env::var(SECRET_TOOL_COMMAND).unwrap_or(String::from("secret-tool"));
    let password = Command::new(&secret_tool_command)
        .args(["lookup", &args.attribute, &args.value])
        .output()
        .map(|output| {
            if output.status.success() {
                String::from_utf8(output.stdout).map_err(|e| e.into())
            } else {
                Err(anyhow!(
                    "{} commanded exited with error{}: {}",
                    secret_tool_command,
                    output
                        .status
                        .code()
                        .map(|i| format!(" code {i}"))
                        .unwrap_or_default(),
                    String::from_utf8_lossy(&output.stderr)
                ))
            }
        })
        // FIXME: use flatten() when Result::flatten() gets stabilized
        .unwrap_or_else(|e| Err(e.into()));
    match password {
        Ok(password) => {
            println!("{password}");
            Ok(())
        }
        _ => {
            // fallback on CLI
            let exit_loop = Rc::new(RefCell::new(false));
            let mut siv = Cursive::new();
            loop {
                if *exit_loop.borrow() {
                    break;
                }
                siv.dump();
                let msg = format!("Enter the password to unlock {}", &args.keepassdb);
                let layout = LinearLayout::vertical().child(TextView::new(&msg)).child(
                    EditView::new()
                        .on_submit(|s, _| {
                            s.quit();
                        })
                        .secret()
                        .with_name("input")
                        .fixed_width(msg.len()),
                );
                // clone Rc to make the borrow-checker happy: only the smart
                // pointer is cloned and the closure given ownership to it,
                // all Rc clones point to the same underlying data
                let exit_flag = Rc::clone(&exit_loop);
                siv.add_layer(Dialog::new().title(&args.keepassdb).content(layout).button(
                    "Cancel",
                    move |s| {
                        // Clear content on Cancel
                        s.call_on_name("input", |view: &mut EditView| view.set_content(""));
                        s.quit();
                        *exit_flag.borrow_mut() = true;
                    },
                ));

                siv.run();
                if !*exit_loop.borrow() {
                    let keepassdb_password = format!(
                        "{}",
                        siv.call_on_name("input", |view: &mut EditView| { view.get_content() })
                            .ok_or(anyhow!("Unable to get password from cursive interface"))?
                    );
                    if keepassdb_password.is_empty() {
                        siv.dump();
                        siv.add_layer(
                            Dialog::new()
                                .content(TextView::new(
                                    "Empty password, please input password or exit",
                                ))
                                .button("Ok", |s| s.quit()),
                        );
                        siv.run();
                    } else {
                        let keepassxc_cli_command = env::var(KEEPASSXC_CLI_COMMAND)
                            .unwrap_or(String::from("keepassxc-cli"));
                        let mut keepassxc_cli = Command::new(&keepassxc_cli_command)
                            .args([
                                "show",
                                "-s",
                                "-a",
                                "password",
                                &args.keepassdb,
                                &args.attribute,
                            ])
                            .stdin(Stdio::piped())
                            .stdout(Stdio::piped())
                            .stderr(Stdio::piped())
                            .spawn()?;

                        let mut stdin = keepassxc_cli
                            .stdin
                            .take()
                            .ok_or(anyhow!("Failed to open stdin"))?;
                        let _result = std::thread::spawn(move || {
                            stdin
                                .write_all(keepassdb_password.as_bytes())
                                .expect("Failed to write to stdin");
                        })
                        .join()
                        .map_err(|e| {
                            anyhow!("Failed to feed password into keepassxc-cli: {e:?}")
                        })?;

                        let output = keepassxc_cli.wait_with_output()?;

                        // wrap up a Result from the command output
                        let password = if output.status.success() {
                            String::from_utf8(output.stdout.clone())
                                .map(|s| {
                                    s.lines()
                                        .last()
                                        .ok_or(anyhow!("No last line in output!"))
                                        .map(|t| t.to_owned())
                                })
                                .map_err(|e| e.into())
                        } else {
                            let err = String::from_utf8_lossy(&output.stderr);
                            let err = if err.contains(&args.keepassdb) {
                                format!(
                                    "{}",
                                    err.split_once('\n').unwrap_or(("", "No error message")).1
                                )
                            } else {
                                err.to_string()
                            };
                            Err(anyhow!(err))
                        }
                        .unwrap_or_else(|e| Err(e));

                        match password {
                            Ok(password) => {
                                println!("{password}");
                                return Ok(());
                            }
                            Err(e) => {
                                let exit_flag = Rc::clone(&exit_loop);
                                // print the error message on TUI
                                siv.dump();
                                siv.add_layer(
                                    Dialog::text(format!("{e}"))
                                        .title("Error message")
                                        .button("Retry", |s| s.quit())
                                        .button("Exit", move |s| {
                                            *exit_flag.borrow_mut() = true;
                                            s.quit()
                                        }),
                                );
                                siv.run();
                            }
                        }
                    }
                }
            }
            Err(anyhow!("Aborted by user"))
        }
    }
}
